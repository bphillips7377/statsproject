

import java.io.*;
import acm.program.*;

public class StatsProj extends ConsoleProgram {

	//Specify the file we are reading in the trainer and testing the data on assumes the suffix is -train.txt and -test.txt
	private final static String FILE_NAME = "heart";
	//Determine if we're using bayes or the logistic model
	private final boolean bayesian = false;
	private final static int NUM_TYPES = 2;
	private final static int TRUE_FALSE = 2;
	private final static int EPOCH = 10000;
	private final static double LEARNING = .0000001;
	private double[][] model;
	private boolean[][] input;

	//Train the model and then test it.
	public void run(){
		trainModel();
		testModel();
	}

	//Update the bayesian model with a given training line
	private void updateModel(String line, int numElems){
		int type = line.charAt(line.length() - 1) - '0';
		for(int i = 0; i < numElems; i++){
			if(line.charAt(i * 2) == '1') {
				model[i * 2 + 1][type]++;
			}
			else model[i * 2][type]++;
		}
	}

	//Store the input from the training data to use in the logistic regression
	private void storeValue(String line, int numElems, int trial){
		input[numElems][trial] = (line.charAt(line.length() - 1) == '1');
		for(int i = 0; i < numElems; i++){
			input[i][trial] = (line.charAt(i * 2) == '1');
		}
	}
	
	//Update the logistic model with EPOCH number of updates.
	private void buildRegression(){
		// "epochs" = number of passes over data during learning
		for (int i = 0; i < EPOCH; i++){
			double[] gradient = new double[model.length];
			// Compute "batch" gradient vector
			for(int trial = 0; trial < input[0].length; trial++){
				// Add contribution to gradient for each data point
				double z = model[0][1];
				for (int elem = 0; elem < input.length - 1; elem++){
					z += (input[elem][trial] ? 1 : 0) * model[elem + 1][1];
				}
				double expression = (input[input.length - 1][trial] ? 1 : 0) - 1/(1 + Math.exp(-z));
				gradient[0] += expression;
				for (int j = 1; j < input.length; j++) {
					// Note: xj below is j-th input variable and x0 = 1.
					gradient[j] += (input[j - 1][trial] ? 1 : 0) * expression;
				}
			}
	// Update all j. Note learning rate h is pre-set constant
			for(int beta = 0; beta < input.length; beta++){
				model[beta][1] += LEARNING * gradient[beta];
			}
		}
	}
	
	//Take in the data from the trainer and build either a bayesian or logistic representation
	private void trainModel(){
		try {
			BufferedReader rd = new BufferedReader(new FileReader(FILE_NAME + "-train.txt"));
			int nPredictors = Integer.parseInt(rd.readLine());
			int numTrain = Integer.parseInt(rd.readLine());
			int factor = TRUE_FALSE;
			if(!bayesian) {
				factor = 1;
				input = new boolean[nPredictors + 1][numTrain];
			}
			model = new double[nPredictors * factor + 1][NUM_TYPES];
			for(int i = 0; i < numTrain; i++){
				if(bayesian) updateModel(rd.readLine(), nPredictors);
				else storeValue(rd.readLine(), nPredictors, i);
			}
			rd.close();
			if(bayesian) finalizeBayesian(nPredictors, numTrain);
			else buildRegression();
		} catch (IOException IO){
			println("Problem: IO Exception");
		}
	}

	//Convert the frequency counts in the model to the logs of their probability
	private void finalizeBayesian(int nPredictors, int numTrain) {
		for(int i = 0; i < nPredictors * 2; i++){
			for(int j = 0; j < NUM_TYPES; j++){
				model[nPredictors * 2][j] += model[i][j];
				model[i][j] = Math.log(model[i][j]) - Math.log(nPredictors * numTrain);
			}
		}
		for(int i = 0; i < NUM_TYPES; i++){
			model[nPredictors * 2][i] = Math.log(model[nPredictors * 2][i]) - Math.log(nPredictors * numTrain);
		}
	}

	//Test the sample with either the bayesian model or the logistic model
	private void testSample(String str, int[][] results, int numElem){
		boolean answer;
		if(bayesian) answer = getBayesianPrediction(str, numElem);
		else answer = getLogisticPrediction(str, numElem);
		analyzeResult(str, results, answer);
	}

	//Analyze whether the answer from the model is what the actual value is. Update the results accordingly
	private void analyzeResult(String str, int[][] results, boolean answer) {
		if(str.charAt(str.length() - 1) == '0'){
			if(!answer) results[0][1]++;
			results[0][0]++;
		} else {
			if(answer) results[1][1]++;
			results[1][0]++;
		}
	}

	//Get the prediction of the answer from the logistic model.
	private boolean getLogisticPrediction(String str, int numElem) {
		double z = model[0][1];
		for(int i = 0; i < numElem; i++){
			if(str.charAt(i * 2) == '1') z += model[i + 1][1];
		}
		if(1/(1+Math.exp(-z)) > .5) return true;
		return false;
	}

	//Get the answer that Bayes provides us.
	private boolean getBayesianPrediction(String str, int numElem) {
		double prediction = model[numElem * 2][0] - model[numElem * 2][1];
		for(int i = 0; i < numElem; i++){
			if(str.charAt(i * 2) == '0'){
				prediction += model[i * 2][0] - model[i * 2][1];
			}
			else prediction += model[i * 2 + 1][0] - model[i * 2 + 1][1];
		}
		if(prediction < 0) return true;
		return false;
	}
	
	//Read in the tester file and check each model for accuracy.
	private int[][] readTester(){
		try {
			BufferedReader rd = new BufferedReader(new FileReader(FILE_NAME + "-test.txt"));
			int[][] results = new int[NUM_TYPES][TRUE_FALSE];
			int nPredictors = Integer.parseInt(rd.readLine());
			int numTest = Integer.parseInt(rd.readLine());
			for(int i = 0; i < numTest; i++){
				testSample(rd.readLine(), results, nPredictors);
			}
			return results;
			
		} catch (IOException IO){
			println("Problem: IO Exception");
		}
		return null;
	}
	
	//Print out the values that we received
	private void printResults(int[][] results){
		for(int i = 0; i < NUM_TYPES; i++){
			println("class " + i + ": tested " + results[i][0] + ", correctly classified " + results[i][1]);
		}
		int total = results[1][0] + results[0][0];
		int correct = results[0][1] + results[1][1];
		println("Overall: tested " + total + ", correctly classified " + correct);
		println("Accuracy = " + ((double) correct) / total);
	}

	//Test how good the model was at predicting the answers and print the results
	private void testModel(){
		int[][] results = readTester();
		printResults(results);

	}
}
